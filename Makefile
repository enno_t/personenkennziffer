CC=gcc
CFLAGS=-Wall -Wextra -std=c99 -pipe -march=native -O2 -D_FORTIFY_SOURCE=2 -fstack-protector-strong -fcf-protection
LIBS=-I/usr/include/gtk-3.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include \
     -I/usr/include/sysprof-4 -I/usr/include/harfbuzz -I/usr/include/freetype2 -I/usr/include/libpng16 \
     -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/fribidi -I/usr/include/cairo \
     -I/usr/include/lzo -I/usr/include/pixman-1 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/gio-unix-2.0 \
     -I/usr/include/cloudproviders -I/usr/include/atk-1.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/dbus-1.0 \
     -I/usr/lib/dbus-1.0/include -I/usr/include/at-spi-2.0 -pthread -lgtk-3 -lgdk-3 -lz -lpangocairo-1.0 \
     -lpango-1.0 -lharfbuzz -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 \
     -rdynamic

PREFIX="/usr/local/bin"

OBJQ = pkenn.o common.o
OBJQ_GTK = pkenn-gtk.o common.o

%.o: %.c
	@echo CC $^
	@$(CC) -c -o $@ $< $(CFLAGS) $(LIBS)

pkenn: $(OBJQ)
	@echo CC $^
	@$(CC) -o $@ $^ $(CFLAGS)

pkenn-gtk: $(OBJQ_GTK)
	@echo CC $^
	@$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

install:
	@echo INSTALL PKENN
	@mv pkenn $(PREFIX)

install-gtk:
	@echo INSTALL PKENN
	@mv pkenn-gtk $(PREFIX)

clean:
	@echo rm OBJECT_FILES
	@rm -f $(OBJQ) $(OBJQ_GTK)
	@echo rm BINARIES
	@rm -f pkenn pkenn-gtk
