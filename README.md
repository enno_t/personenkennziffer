# Personenkennziffer

A program to generate the PZ check digit for a Bundeswehr Personenkennziffer

## Installation

Console interface: `make`

GTK interface: `make pkenn-gtk`

And then: `make install` or `make install-gtk`

## Usage

`personenkennziffer PERSONENKENNZIFFER [...PERSONENKENNZIFFER...]`


## License
GPL-3.0-or-later
