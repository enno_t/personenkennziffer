/*
*   SPDX-FileCopyrightText: 2022 enno_t <enno@tensing.me>
*   SPDX-License-Identifier: GPL-3.0-or-later
*   common.c - Code shared between interfaces
*   Copyright (C) 2022  enno_t
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License Version 3 or
*   later as published by the Free Software Foundation.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License or the LICENSE file for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "common.h"
#include <string.h>

static int MULTIPLIER[] = { 2, 3, 4, 5, 6, 7, 1, 6, 7, 2, 3 };

int _atoi(char c)
{
	// 'a' is 'A' + (26 + 6), so if that
	// is met subtract (26 + 6) to get
	// only uppercase chars
	if (c >= (64 + 26 + 6))
		c = (char) c - (26 + 6);
	switch (c) {
	case 'S':
		return 4;
	case 'J':
	case 'T':
		return 6;
	case 'K':
	case 'U':
		return 8;
	case 'L':
	case 'V':
		return 10;
	case 'A':
	case 'M':
	case 'W':
		return 12;
	case 'B':
	case 'N':
	case 'X':
		return 14;
	case 'C':
	case 'O':
	case 'Y':
		return 16;
	case 'D':
	case 'P':
	case 'Z':
		return 18;
	case 'E':
	case 'Q':
		return 20;
	case 'F':
	case 'R':
		return 22;
	case 'G':
		return 24;
	case 'H':
		return 26;
	case 'I':
		return 28;
	default:
		// 0-9 are also chars and thus we have
		// to subtract 48 to get their face value
		return c - 48;
	}
}

int pkenn(char *s)
{
	int skipped = 0;
	int len = strlen(s);
	int sum = 0;
	if (len < 10)
		return -1;

	for (int i = 0; i < len; ++i) {
		if (s[i] == '-') {
			++skipped;
			continue;
		}

		if (skipped)
			sum += _atoi(s[i]) * MULTIPLIER[i - skipped];
		else
			sum += _atoi(s[i]) * MULTIPLIER[i];
	}
	return 11 - (sum % 11);
}

