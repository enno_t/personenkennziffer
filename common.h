/*
*   SPDX-FileCopyrightText: 2022 enno_t <enno@tensing.me>
*   SPDX-License-Identifier: GPL-3.0-or-later
*   common.h - Code Shared between interfaces
*   Copyright (C) 2022  enno_t
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License Version 3 or
*   later as published by the Free Software Foundation.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License or the LICENSE file for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COMMON_H
#define COMMON_H

#define NORMALISE(x) ((x > 10) ? (x - 10) : (x))
#define TO_ASCII(x) (NORMALISE(x) + 48)

int _atoi(char);
int pkenn(char *);

#endif
