/*
*   SPDX-FileCopyrightText: 2022 enno_t <enno@tensing.me>
*   SPDX-License-Identifier: GPL-3.0-or-later
*   pkenn-gtk.c - A GTK interface to pkenn
*   Copyright (C) 2022  enno_t
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License Version 3 or
*   later as published by the Free Software Foundation.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License or the LICENSE file for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include <gtk/gtk.h>
#include "common.h"

GtkWidget *result;
GtkWidget *result_label;
GtkWidget *result_btn;

void on_btn_clicked(GtkWidget *widget, gpointer data)
{
	int pz = pkenn(gtk_entry_get_text(GTK_ENTRY(data)));
	if (pz != -1) {
		char c = TO_ASCII(pz);
		gtk_label_set_text(GTK_LABEL(result_label), &c);
	} else {
		gtk_label_set_text(GTK_LABEL(result_label), "Fehlerhafte Eingabe");
	}
	gtk_widget_show_all(result);
}

void on_result_btn_clicked(GtkWidget *widget, gpointer data)
{
	gtk_widget_hide(result_label);
	gtk_widget_hide(result_btn);
	gtk_widget_hide(result);
}

int main(int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *entry;
	GtkWidget *btn;
	GtkBuilder *builder = NULL;
	gtk_init(&argc, &argv);

	builder = gtk_builder_new();

	if (gtk_builder_add_from_file(builder, "interface.glade", NULL) == 0) {
		fprintf(stderr, "gtk_builder_add_from_file failed\n");
		return 1;
	}

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	result = GTK_WIDGET(gtk_builder_get_object(builder, "result_window"));
	entry = GTK_WIDGET(gtk_builder_get_object(builder, "entry"));
	btn = GTK_WIDGET(gtk_builder_get_object(builder, "btn"));
	result_label = GTK_WIDGET(gtk_builder_get_object(builder, "result_label"));
	result_btn = GTK_WIDGET(gtk_builder_get_object(builder, "result_btn"));
	gtk_builder_connect_signals(builder, NULL);
	gtk_widget_show_all(window);
	gtk_main();

	g_object_unref(builder);
	return 0;
}
