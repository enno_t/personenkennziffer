/*
*   SPDX-FileCopyrightText: 2022 enno_t <enno@tensing.me>
*   SPDX-License-Identifier: GPL-3.0-or-later
*   pkenn.c - A simple program to calcualte a check digit
*   Copyright (C) 2022  enno_t
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License Version 3 or
*   later as published by the Free Software Foundation.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License or the LICENSE file for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

int main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "%s INPUT", argv[0]);
		return 1;
	}

	int pz = -1;

	// Calculate the PZ for every arg
	for (int i = 1; i < argc; ++i) {
		pz = pkenn(argv[i]);
		// A valid input is at least 11 characters long.
		// For now just exit if that is not the case.
		
		if (pz == -1) {
			fprintf(stderr, "%s is not a valid input\n", argv[i]);
			return 1;
		}

		printf("%d\n", pz);
	}

	return 0;
}
